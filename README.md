# Game of Dice Wiki

Our wiki is a custom developed PHP/MySQL based system. It makes use of a few composer libraries and utilizes Discord OAuth for user management. 


## Wiki Installation
* Clone/download the repo
* Setup your database in `inc/db.php` and import it
* Setup your keys and update URLs
  * Discord
    * `inc/users.class.php:8` - client_id
    * `inc/users.class.php:9` - client_key
    * `inc/users.class.php:10` - returnuri
   * RemoveBg
     * `inc/cms.class.php:289`
  * TinyMCE
     * `pages/modals.php:7`
  * search-replace `gameofdice.wiki` with your URL


Use [composer](https://pip.pypa.io/en/stable/) to install the vendor files.

```bash
composer install
```

## Using the wiki

Currently in order to create and edit pages, you must be an editor (Rank: 10) or an admin of the site (Rank: 420) which is automatically assigned by the Bot when using `!lookup` commands. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

-  MyNode#0420 on Discord
