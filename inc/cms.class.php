<?php
use Mtownsend\RemoveBg\RemoveBg;
class webcms {

  public function __construct() {
    if(file_exists('inc/db.php')) {
    include "inc/db.php";
      if($dbCon['enabled'] === TRUE) {
        include "inc/dbClass.php";
        $this->db = new DB();
        DB::$user = $dbCon['username'];
        DB::$password = $dbCon['password'];
        DB::$dbName = $dbCon['dbname'];
        #if(class_exists('DB')) { echo "DB is active <br />"; }
      } else {
        # We have database connection details, but they're not enabled.
        #echo "Database Connection details exist, but they're not enabled";
        return FALSE;
      }
    } else {
      # We have no database connection details, no sense moving forward
      #echo "Database is disabled";
      return FALSE;
    }

    # Lets not forget to set the fucking time zone
    date_default_timezone_set('America/Denver');
  }

  public static function dbactive() {
    if(class_exists('DB')) { return TRUE; } else { return FALSE; }
  }

  public static function adminWorld() {
    if($this->dbactive()) {
      global $settings;
      $settings['style'] = 'theme';
    } else {
      # set a page to alert that there's no admin panel without a database
      $this->errorPage('There can be no admin panel with no database connection.');
    }
  }

  public static function errorPage($msg) {
    error_log($msg);
    $_SESSION['error_message'] = $msg;
    require_once('pages/error.php');
    require_once('style/theme/footer.php');
    exit();
  }

  public static function test() {
    $this->getPage(1);
  }
  
  public static function pageExist($id) {
     $ck = DB::query("SELECT * FROM `pages` WHERE `id` = %i or `crumb` = %s", $id, $id);
    if(count($ck) == 0) { return FALSE; } else { return TRUE; }
  }
  
  public static function getPage($id, $category='') {
    if(!self::dbactive()) { $this->errorPage('No active database'); }
    if(!self::pageExist($id)) { return false; } else {
        if(!empty($category)) {
          $ck = DB::query("SELECT * FROM `pages` WHERE (`crumb` = %s and `category` = %s)", $id, $category);
        } else {
         $ck = DB::query("SELECT * FROM `pages` WHERE (`crumb` = %s and `category` = '')" , $id);
        }
          return $ck['0'];
    }
  }
  public static function getItem($id, $type, $class) {
    if(!self::dbactive()) { $this->errorPage('No active database'); }
         $ck = DB::query("SELECT * FROM `items` where `class` = %s AND `type` = %s AND `name` = %s", $class, $type, $id);
          return $ck['0'];
  }
  public static function pageLoginOnly($id) {
    if(!self::dbactive()) { $this->errorPage('No active database'); }
    if(!self::pageExist($id)) { $this->errorPage('Page does not exist'); } else {
      $q = DB::queryFirstRow("SELECT `loggedinonly` FROM pages WHERE `id` = %i or `crumb` = %s", $id, $id);
        if($q['loggedinonly'] == 1) {
          return TRUE;
        } else {
          return FALSE;
        }
    }
  }

  public static function successMessage($msg) {
    echo '<div class="row">
            <div class="col-lg-12"><div class="alert alert-success" style="border-radius:8px;">
            <button type="button" aria-hidden="true" class="close">×</button>
            <span>'.$msg.'</span>
          </div></div></div>';
  }

  protected static function sendMail($to, $subject, $message) {
    mail($to, $subject, $message);
  }
  
 private static function isMd5($md5 ='') {
    return strlen($md5) == 32 && ctype_xdigit($md5);
  }

  public static function purify(string $content) : string
  {
    $config = \HTMLPurifier_Config::createDefault();
    $config->set('HTML.AllowedElements', [
      'a',
      'b',
      'blockquote',
      'br',
      'code',
      'em',
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'hr',
      'i',
      'img',
      'li',
      'ol',
      'p',
      'pre',
      's',
      'span',
      'strong',
      'sub',
      'sup',
      'table',
      'tbody',
      'td',
      'th',
      'thead',
      'tr',
      'u',
      'ul',
      'div'
    ]);
    $config->set('CSS.AllowedProperties', [
      'color',
      'background-color',
      'font-size',
      'font-weight',
      'padding-left',
      'text-align',
      'text-decoration',
    ]);
    $config->set('Attr.AllowedClasses', [
      'alert',
      'alert-danger',
      'alert-info',
      'alert-success',
      'alert-warning',
      'blockquote',
      'blockquote-footer',
      'img-responsive',
      'table',
      'table-bordered',
      'table-condensed',
      'table-hover',
      'table-responsive',
      'table-striped',
    ]);
    $config->set('Attr.AllowedFrameTargets', [
      '_blank',
    ]);
    $config->set('HTML.AllowedAttributes', [
      'a.href',
      'a.target',
      'code.class',
      'img.class',
      'img.src',
      'ol.start',
      'p.class',
      'p.style',
      'span.style',
      'table.class',
      'td.align',
      'th.align',
    ]);
    $config->set('URI.AllowedSchemes', [
      'http' => true,
      'https' => true,
    ]);
    return (new \HTMLPurifier())->purify($content, $config);
  }

  public function createPage($title, $content, $crumb, $options=[]) {
    if(!is_array($options)) { return FALSE; } # Options can only be an array
    if(empty($title) || empty($content) || empty($crumb)) { return FALSE; } # Something was left out
    # Check for a category in our crumbs
    $crumb = utf8_encode($crumb);
    if(strpos($crumb, '/')) {
      $category = explode('/', $crumb)[0];
      $crumb = explode('/', $crumb)[1];
    }
    if(!$category) { $category = ''; }
    if(self::getPage($crumb, $category)) {
      return FALSE; # Page already exists
    }
    $title = self::purify(utf8_encode($title));
    $content = self::purify(utf8_encode($content));
    if(!$options['adminonly']) {
      $options['adminonly'] = 0;
    }
    if(!$options['editlock']) {
      $options['editlock'] = 0;
    }
    if(!$options['loginreq']) {
      $options['loginreq'] = 0;
    }
    if(!$options['html']) {
      $options['html'] = 0;
    }

    
    DB::insert("pages", [
      'title' => $title,
      'crumb' => $crumb,
      'category' => $category,
      'content' => $content,
      'creator' => $_SESSION['profile']['user'],
      'lastedit' => time(),
      'loggedinonly' => $options['loginreq'],
      'editlock' => $options['editlock'],
      'adminonly' => $options['adminonly'],
      'html' => $options['html']
    ]);
    if(DB::query("SELECT * FROM `pages` WHERE `crumb` = %s AND `category` = %s", $crumb, $category)) {
      header('Location: https://'.$_SERVER['HTTP_HOST'].'/'.$category.'/'.$crumb);
    $_SESSION['echo'] = "<div class='row'><div class='col-xl-6 col-lg-12 col-md-12 mb-4'><div class='card bg-info text-white shadow'>
    <div class='card-body'>Page successfully created!</div></div></div></div>";
    }
  }

  public function editPage($title, $content, $crumb, $options=[]) {
    if(!is_array($options)) { return FALSE; } # Options can only be an array
    if(empty($title) || empty($content) || empty($crumb)) { return FALSE; } # Something was left out
    # Check for a category in our crumbs
    $crumb = utf8_encode($crumb);
    if(strpos($crumb, '/')) {
      $category = explode('/', $crumb)[0];
      $crumb = explode('/', $crumb)[1];
    }
    if(!$category) { $category = ''; }
    if(!self::getPage($crumb, $category)) {
      return FALSE; # Page doesnt exist
    }
    $title = self::purify(utf8_encode($title));
    $content = self::purify(utf8_encode($content));
    if(!$options['adminonly']) {
      $options['adminonly'] = 0;
    }
    if(!$options['editlock']) {
      $options['editlock'] = 0;
    }
    if(!$options['loginreq']) {
      $options['loginreq'] = 0;
    }
    if(!$options['html']) {
      $options['html'] = 0;
    }
    $time = time();
    DB::update('pages',
    [
      'title' => $title,
      'crumb' => $crumb,
      'category' => $category,
      'content' => $content,
      'lastedit' => $time,
      'loggedinonly' => $options['loginreq'],
      'editlock' => $options['editlock'],
      'adminonly' => $options['adminonly'],
      'html' => $options['html']
  ], "id = %i", self::getPage($crumb, $category)['id']);
  if(DB::query("SELECT * FROM `pages` WHERE `lastedit` = %i", $time)) {
      header('Location: https://'.$_SERVER['HTTP_HOST'].'/'.$category.'/'.$crumb);
      $_SESSION['echo'] = "<div class='row'><div class='col-xl-6 col-lg-12 col-md-12 mb-4'><div class='card bg-info text-white shadow'>
    <div class='card-body'>Page successfully edited!</div></div></div></div>";
  }
  }

  public function transparentImage($item, $url='') {
    if(!file_exists('media/'.$item.'.png')) {
      $removebg = new RemoveBg('**FILTERED - removebg-api**');
      $removebg->url($url)->save("media/$item.png");
      DB::update('items',[
        'image' => 'https://gameofdice.wiki/media/'.$item.'.png'
        ], "name = %s", $item);
    }
    if(file_exists('media/'.$item.'.png')) {
      return '/media/'.$item.'.png';
    } else {
      return '/media/404.png';
    }
  }

}

?>
