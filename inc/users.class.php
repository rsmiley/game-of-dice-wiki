<?php
/* users.class.php
Manages all things user related
Author: Roger Smiley
*/
class users extends webcms {
	function __construct() {
		$this->client_id = '**FILTERED - discord-cid**'; # Predefined
        $this->client_key = '**FILTERED - discord-ck**'; # Predefined
        $this->returnuri = 'https://gameofdice.wiki/login/'; # Customizable
        $this->scopes = 'identify guilds email'; # Customizable
        $this->access_token = ''; # Preset for future use

        $this->authurl = 'https://discord.com/api/oauth2/authorize';
        $this->tokenurl = 'https://discord.com/api/v8/oauth2/token';
        $this->apibase = 'https://discord.com/api/v8/users/@me';
	}

	public static function userExists($user) {
		$check = DB::queryFirstRow("SELECT `user` FROM `users` WHERE `user` = %s or `id` = %i", $user, $user);
		if(is_null($check)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

  public static function getUserID($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getUserID in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `id` FROM `users` WHERE `user` = %s or `id` = %i", $user, $user);
		if(!is_null($get)) {
			return $get['id'];
		} else {
			return FALSE;
		}
	}
  
	public static function getUser($user) {
		# Full user array
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getUser in users.class.php"); }
		$get = DB::queryFirstRow("SELECT * FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get;
		} else {
			return FALSE;
		}
	}

	public static function getEmail($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getEmail in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `email` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['email'];
		} else {
			return FALSE;
		}
	}
  
	public static function getRank($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getRank in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `rank` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['rank'];
		} else {
			return FALSE;
		}
	}

	public static function getToken($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getToken in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `token` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['token'];
		} else {
			return FALSE;
		}
	}

	public static function getApiToken($user) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getApiToken in users.class.php"); }
		$get = DB::queryFirstRow("SELECT `apitoken` FROM `users` WHERE `user` = %s", $user);
		if(!is_null($get)) {
			return $get['apitoken'];
		} else {
			return FALSE;
		}
	}
	
	public static function get($user, $item) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for getEmail in users.class.php"); }
		$columns = DB::columnList('users');
		if(!in_array($item, $columns)) { return parent::errorPage("You are trying to update an invalid column. Col: $item"); }
	  return DB::queryFirstRow("SELECT `$item` from `users` where `id` = %i", self::getUserID($user))[$item];  
	}

	public static function createUser($user, $pass, $email, $extra=array()) {
		# Already registered?
		$regCheck = DB::queryFirstRow("SELECT `user`FROM `users` WHERE `user` = %s or `email` = %s", $user, $email);
		if(!is_null($regCheck)) { 
			# They have an account already, lets log them in.
			self::generateSignOnLink($regCheck['user'], 'login');
			echo "<center>A user with that name or email address already exists in our database.</center>";
			exit();
		}
    if(empty($user) || empty($pass) || empty($email)) { return parent::errorPage("You must fill out all of the fields."); }
		# We made it this far, we must know them.
		#exit("<center>Invalid user<br /><b>Registration is not available</b></center>"); # Nope.
    
		DB::insert('users', array(
			'user' => $user,
      'password' => password_hash($pass, PASSWORD_DEFAULT),
			'email' => $email,
			'activated' => 0
			));
		$emailMessage = "Your registration has been confirmed, but in order to continue you must verify your email address.

Simply click the link below to activate and login to your new account!
".self::generateSignOnLink($user, 'register');
		self::sendMail($email, "MyNode Manager - Account Created", $emailMessage);
    return true;
#		echo "Your account has been created! <br />Please verify your email address as well to receive updates and promotional emails.";
#		exit();
	}

	public static function login($user, $skip=FALSE) {
    if(!self::userExists($user)) { return parent::errorPage("User does not exist for login() in users.class.php"); }
    if($skip != TRUE) {
      $compare = DB::queryFirstRow("SELECT `user` FROM `users` WHERE `user` = %s", $user);
      if(is_null($compare)) { return parent::errorPage('Invalid Username/Password'); }

      if(self::getUser($user)['activated'] == 0) {
        # If they're not activated, activate them and run the creation steps.
        DB::update('users', array(
          'activated' => 1), "`user` = %s", $user);
        self::createUserProcess($user); # We could use this for promotions and shit lol
      }
    } # end our skip hack
		# Tokens match, lets give them a welcome token
		$newToken = self::generateToken();
		self::updateUser($user, array('token', 'activated'), array($newToken, '1'));
		setcookie("mynodeToken", $newToken, strtotime('+60 days'), '/');
	}

	public static function logout() {
		setcookie('mynodeToken', '', strtotime('-1 year'), '/');
		unset($_SESSION['profile']);
	}

	public static function isLoggedIn() {
		if(isset($_SESSION['profile'])) {
			return TRUE;
		} else {
			if(isset($_COOKIE['mynodeToken'])) {
				# Question the token.
				$find = DB::queryFirstRow("SELECT `user` FROM `users` WHERE `token` = %s", $_COOKIE['mynodeToken']);
				if(is_null($find)) {
					# We did not find any matching token. Kill the cookie!
					# I know we shouldn't suppress errors, but it's gonna send errors until it loads before the html which will eventually happen.
					@self::logout();
					return FALSE;
				} else {
					# $_SESSION['profile'] wasn't set, that's how we got here. Let's build it.
					$getUser = self::getUser($find['user']);
					foreach($getUser as $k => $v) {
						$_SESSION['profile'][$k] = $v;
					}
					return TRUE;
				}
			} else {
				# We have no upload token cookie, not logged in
				return FALSE;
			}
		}
	}
  
  public static function isAdmin() { 
    if(!self::isLoggedIn()) { return FALSE; }
    	switch(self::getRank($_SESSION['profile']['user'])) {
        case '420':
			# Discord Admin
          return TRUE;
          break;
        default:
          return FALSE;
          break;
      }
  }
  public static function isEditor() { 
    if(!self::isLoggedIn()) { return FALSE; }
    	switch(self::getRank($_SESSION['profile']['user'])) {
        case '10':
			# Wiki Editor
          return TRUE;
          break;
		case '420':
			# Discord Admin
          return TRUE;
          break;
        default:
          return FALSE;
          break;
      }
  }

	protected static function updateUser($user, $row, $val) {
		# This expects both $row and $val to be arrays.
		if(!self::userExists($user)) { return parent::errorPage("User does not exist for updateUser in users.class.php"); }		
		$columns = DB::columnList('users');
	    $updates = array_combine($row, $val);
	    foreach($updates as $r => $k) {
	      if(!in_array($r, $columns)) { return parent::errorPage("You are trying to update an invalid column."); }
	        DB::update("users", array(
	      $r => $k), "`user` = %s or `id` = %s", $user, $user);
	    }
	}

	protected static function generateToken() {
		/* Stolen from StackOverflow but idr the post */
		$source = file_get_contents('/dev/urandom', false, null, null, 64);
        $source .= uniqid(uniqid(mt_rand(0, PHP_INT_MAX), true), true);
        for ($t=0; $t<64; $t++) {
            $source .= chr((mt_rand() ^ mt_rand()) % 256);
        }
        $return = sha1(hash('sha512', $source, true));
        return $return;
	}

	public static function generateSignOnLink($user, $type) {
		if(!self::userExists($user)) { return parent::errorPage("User does not exist to email."); }

		switch($type) {
			case 'login':
				# Login
				# Create url, send email, and return that we did it
				$token = self::generateToken();
				DB::insert('tokenUrls', array(
					'user' => $user,
					'type' => 'login',
					'token' => $token,
					'time' => time()
					));
				$url = "https://my.mynode.co/login/?t={$token}&u={$user}";
			break;

			case 'register':
				# Register
				# Provide the URL for the email and return it
				$token = self::generateToken();
				DB::insert('tokenUrls', array(
					'user' => $user,
					'type' => 'register',
					'token' => $token,
					'time' => time()
					));
				$url = "https://my.mynode.co/register/?t={$token}&u={$user}";
			break;
		}
	}

    	protected static function generateSecureUploadToken($user) {
		# This just needs to be complex enough to be secure, but difficult enough that it won't be fun to share.
		return self::generateToken(); # It's quite secure anyway.
	}

	protected static function createUserProcess($user) {
		# Assign the user an API token
		self::updateUser($user, array('apitoken'), array(self::generateToken()));
	}
  
  public static function userAction() {
    # This function handles the user level up, exp usage, etc for the account
    # This may get fine tuned to allow for specific changes to a user, but keep leveling / exp usage as a constant action unless paused.
  }

  private function sendAPIreq($request) {
	if(empty($request)) { return; }
	if(is_array($request)) { json_encode($request); }
	Requests::get();
}

public function calllogin() {
	$params = array(
		'client_id' => $this->client_id,
		'redirect_uri' => $this->returnuri,
		'response_type' => 'code',
		'scope' => $this->scopes
	);
	  header('Location: ' . $this->authurl . '?' . http_build_query($params));
	  return;
}

public function process($code) {
	$headers = [ 
		'Accept' => 'application/json',
		'User-Agent' => 'MyNode Applications',
		'Content-Type' => 'application/x-www-form-urlencoded'
	];
	$data = [
		'client_id' => $this->client_id,
		'client_secret' => $this->client_key,
		'grant_type' => "authorization_code",
		'redirect_uri' => $this->returnuri,
		'code' => $code,
		'scope' => $this->scopes
	];
	$run = Requests::post($this->tokenurl, $headers, $data);
	$access_key_http = $run->body;
	if(isset(json_decode($access_key_http, true)['access_token'])) {
		$_SESSION['discord']['token'] = json_decode($access_key_http, true)['access_token'];
		$headers = [ 
			'Accept' => 'application/json',
			'User-Agent' => 'MyNode Applications',
			'Content-Type' => 'application/x-www-form-urlencoded',
			'Authorization' => 'Bearer '.$_SESSION['discord']['token']
		];
		$user = json_decode(Requests::get($this->apibase, $headers)->body, true);
		/*		Array
(
    [id] => 367495122277105674
    [username] => MyNode
    [avatar] => 70404af5519355850d569d36791fa798
    [discriminator] => 0420
    [public_flags] => 64
    [flags] => 64
    [locale] => en-US
    [mfa_enabled] => 1
    [premium_type] => 2
    [email] => rsmiley@pm.me
    [verified] => 1
)*/
		if(self::userExists($user['id'])) {
			// user already exists
			self::login($user['id'], TRUE);
			self::updateUser($user['id'], ['user', 'discrim', 'email'], [$user['username'], $user['discriminator'], $user['email']]);
			self::updateUser($user['id'], ['access_token'], [$_SESSION['discord']['token']]);
			header('Location: /');
		} else {
			DB::insert('users', array(
				'id' => $user['id'],
				'user' => $user['username'],
				'email' => $user['email'],
				'activated' => 1,
				'access_token' => $_SESSION['discord']['token'],
				'discrim' => $user['discriminator'],
				'email' => $user['email'],
				'token' => ''
				));
				self::login($user['id'], TRUE);
				header('Location: /');
			}
	}
}

public static function getDiscordUser() {
	if(!self::isLoggedIn()) { return; }
	if(!isset($_SESSION['discord']['token'])) {
		$_SESSION['discord']['token'] = self::getUser($_SESSION['profile']['id'])['access_token'];
	}
		$data = json_decode(Requests::get('https://discord.com/api/v8/users/@me', ['Authorization' => 'Bearer '.$_SESSION['discord']['token'], 'Content-Type' => 'application/x-www-form-urlencoded'])->body, true);
	return $data;
}
	
}



?>
