<?php
# index.php
# Written by Roger Smiley
session_start();

/* Classes and their variables */
include "vendor/autoload.php";
require_once("inc/cms.class.php");
require_once("inc/users.class.php");
$cms = new webcms();
$users = new users();

/* Script Settings */
$settings['style'] = 'theme';

# This is the file based page system
if(!in_array(strtolower(trim($_GET['action'])), ['view', 'edit', 'manage', 'create', 'lock'])) {
  # In case no action gets properly set because of direct links without `view` in them
  $_GET['action'] = 'view';
}
if(in_array(strtolower(trim($_GET['category'])), ['view', 'edit', 'manage', 'create', 'lock'])) {
  # In case categories get set to actions
  $_GET['category'] = '';
}

# Pages that are ALWAYS accessible and have no override
  switch($_GET['page']) {
    # Game Items
      case 'cards':
      case 'goods':
      case 'chars':
      case 'dice':
        $page = 'tables.php';
      break;
      default:
        $page = 'home.php';
      break;
  }

  # Pages the users must be logged to access begins here
    if($users->isLoggedIn()) {
      # Check Discord Session
      if($users->getDiscordUser()['message'] == '401: Unauthorized') {
        $users->logout();
        header('Location: /logout');
      }
    
    # Only allow logged in users to send create/edit action commands
    # Check if the submit exists and we're not working with a game item
    #if(isset($_POST['submit']) && !in_array($_GET['category'], ['cards','goods','chars','dice'])) {
    if(isset($_POST['submit'])) {
        switch($_GET['action']) {
          case 'create':
            if(!is_array($_POST['options'])) { $_POST['options'] = []; }
            $cms->createPage($_POST['title'], $_POST['content'], $_POST['crumb'], $_POST['options']);
          break;
          case 'edit':
            if(!is_array($_POST['options'])) { $_POST['options'] = []; }
            $cms->editPage($_POST['title'], $_POST['content'], $_POST['crumb'], $_POST['options']);
          break;
        }
      }

      /*
      LOGIN MANAGEMENT PAGES
      * This switch and section of the if() statement control
      * what pages are SPECIFICALLY for logged in / logged out users
      */
      # User Pages
        switch($_GET['page']) {
            case 'logout':
              $users->logout();
              sleep(1);
                header('Location: /logout-2');
            break;
            case 'logout-2':
              $users->logout();
              header('Location: /');
            break;
        }
        # End User Pages
    } else {
        switch($_GET['page']) {    
            # Publicly accessible pages only.
            case 'logout-2':
              $users->logout();
              header('Location: /');
              break;
            case 'login':
              if($users->isLoggedIn()) { $users->logout(); header('Location: '.$_SERVER['REQUEST_URI']); } # Prevent any double logging or mistakes.
                if(isset($_GET['code'])) {
                  $users->process($_GET['code']);
                } else {
                  $users->calllogin();
                }
                $page = 'home.php';
            break;
            
            case 'register':
              if($users->isLoggedIn()) { $users->logout(); header('Location: '.$_SERVER['REQUEST_URI']); } # Prevent any double logging or mistakes.
              $users->calllogin();
              $page = 'home.php'; # This is purely to prevent errors should Header errors arise.
            break;
        }
    }

    
# This is the file based page system


# If database is active, this is the dynamic page system
if($page == 'home.php' && ($cms->dbactive() === TRUE && $_SERVER['REQUEST_URI']	!= '/')) {
  # The logic behind this is that if $page is still home, they're either hpmepage or db driven
  #$cms->adminWorld();
  $page = 'temp.php';
  if(in_array($_GET['category'], ['cards','goods','chars','dice'])) {
    $types = [
      '3*',
      '4*',
      '5*',
      '5G',
      '5P',
      '5D',
      '6*',
      '6G',
      '6G+',
      '6P',
      '6P+',
      'black',
      'inf', 'infinity',
      'seasonal', // latest seasonal version
      'collector', 'col' // alias col
    ];
    $found = 0;
    foreach(explode('-', $_GET['page']) as $k) {
      if(in_array($k, $types)) {
        $page = 'lookup.php';
        $found = 1;
      }
    }
    if($found == 0) {
      $cms->errorPage("Unable to locate a type for your {$_GET['page']} lookup");
    }
  }
}
# If database is active, this is the dynamic page system

#print_r($_SESSION);
#print_r($_GET);
#print_r($users->getDiscordUser());

#die('CMS Disabled at this time');
###########################################################
# Below will house the actual construction of the page    #
###########################################################
ob_start();
if(file_exists('style/'.$settings['style'].'/header.php')) {
  require_once("style/{$settings['style']}/header.php");
} else {
  die('Style header is invalid, please be sure the headers named <b>header.php</b>');
}
if(file_exists("pages/{$page}")) {
  require_once("pages/{$page}");
} else {
  create_error('<b>We could not find the file you are looking for, I wonder if it even exists.</b>');
}
if(file_exists('style/'.$settings['style'].'/footer.php')) {
  require_once("style/{$settings['style']}/footer.php");
  # Include the action modals
    if(in_array(strtolower(trim($_GET['action'])), ['edit', 'manage', 'create', 'lock']) && $users->isLoggedIn()) {
      require_once('pages/modals.php');
    }
} else {
  die('Style footer is invalid, please be sure the footer named <b>footer.php</b>');
}
include "serve.php";
?>
