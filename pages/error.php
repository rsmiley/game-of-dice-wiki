<?php
# error.php - reserved for critical errors
session_start();

if(isset($_SESSION['error_message'])) {
$error_msg = $_SESSION['error_message'];
if($_SESSION['error_message'] == '') {
$error_msg = 'We don\'t seem to have an explaination as to why you\'re here but you are!<br /><i>Hello~</i>';
}
} else {
# Do nothing, its not our page
$error_msg .= 'It seems you\'re trying to access this page directly, yea. You can\'t do that!';
}

echo $error_msg;
unset($_SESSION['error_message']);