<?php
# temp.php
# [Template File] | Index Page | Written by Roger Smiley
$page = $cms->getPage($_GET['page'], $_GET['category']);
if(!$page) {
  if($users->isAdmin() || $users->isEditor()) {
      if(!empty($_GET['category'])) { $pagecat = $_GET['category'].'/'; }
  print "<div class='row'><div class='col-xl-6 col-lg-12 col-md-12 mb-4'><div class='card bg-info text-white shadow'>
  <div class='card-body'><b>Admin Notice:</b> The page does not exist, want to <a href='/create/".$pagecat.urlencode($_GET['page'])."/'>create it</a>?</div></div></div></div>";
  } else {
      print "<div class='row'><div class='col-xl-6 col-lg-12 col-md-12 mb-1'><div class='card bg-info text-white shadow'>
      <div class='card-body'>This page is not yet unique. Perhaps you'd like to <a href='/create/".$pagecat.urlencode($_GET['page'])."/'>create it</a>?<br />
      <small>This means the page is automatically generated based on the info we have</small></div></div></div></div>";
  }
}
$type = explode('-', $_GET['page']);
$types = [
    '3*',
    '4*',
    '5*',
    '5G',
    '5P',
    '5D',
    '6*',
    '6G',
    '6G+',
    '6P',
    '6P+',
    'black',
    'inf', 'infinity',
    'seasonal', // latest seasonal version
    'collector', 'col' // alias col
];
foreach($type as $k => $v) {
    if(in_array($v, $types)) {
        $type = $v;
        $page2 = str_ireplace($v, '', $_GET['page']);
    }
}
$item = $cms->getItem(trim(str_ireplace('-', ' ', $page2)), $type, $_GET['category']);

if(!$page) {
$page['title'] = $item['name'];
$page['content'] = "<blockquote>{$item['description']}</blockquote>";
}
/*
    [id] => 1
    [name] => Test Character
    [class] => chars
    [type] => 5*
    [subtype] => NULL
    [image] => https://cdn.discordapp.com/emojis/729508148553056316.gif?v=1
    [description] => if you saw test good, you know im right
    [extra] => {}
    [lastupdate] => 1
    [editor] => 
*/
?>

<p>lookup.php - <?php echo $page2; echo trim(str_ireplace('-', ' ', $page2))."$type, {$_GET['category']}"; ?></p>
<div class="row">
    <div class="col-lg-4">
        <div class="card shadow border-info mb-2">
            <div class="card-header text-center">
                <b><?php echo $page['title']; ?></b>
            </div>
            <img class="card-img-top" src="<?php echo $cms->transparentImage($item['type'].'-'.$item['name'], $item['image']); ?>" alt="Card image cap">
        </div>
    </div>
    <div class="col-lg-8">
    <?php if(isset($_SESSION['echo'])) { echo $_SESSION['echo']; unset($_SESSION['echo']); } ?>
        <div class="card shadow border-info mb-2">
            <div class="card-header">
                <b>Item Description</b>
            </div>
        <div class="card-body">
            <?php echo $page['content']; ?>
        </div>
    </div>
</div>
