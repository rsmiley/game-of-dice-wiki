<?php
$page = $_GET['page'];
$category = $_GET['category'];
$pagecheck = $cms->getPage($_GET['page'], $_GET['category']);

# TinyMCE
print '<script src="https://cdn.tiny.cloud/1/**FILTERED - tiny-api**/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>';
print "  <script>
  tinymce.init({
    selector: 'textarea',
    menubar: true,
    plugins: 'link image code table lists',
    toolbar: 'undo redo | styleselect | forecolor | bold italic | alignleft aligncenter alignright alignjustify | outdent indent table numlist bullist | link image | code'
});
</script>";

$tips = "<blockquote><b>Protip:</b> Use the Source Code option to use <a href='https://getbootstrap.com/docs/4.0/layout/grid/' target='_blank'>Bootstrap 4 rows/grids</a></blockquote>";

if($_GET['action'] == 'edit') {
        if(!$pagecheck) {
        # Page doesn't exist, show the create dialog instead
        header('Location: '.str_replace('edit', 'create', $_SERVER['REQUEST_URI']));
        exit();
    }
    if(!$users->isEditor() && $pagecheck['editlock'] == 1) {
      header('Location: /page-locked/');
    }

    $adminonly = '';
    $editlock = '';
    if(!empty($pagecheck['category'])) {
        $crumb = $pagecheck['category'].'/'.$pagecheck['crumb'];
    } else {
        $crumb = $pagecheck['crumb'];
    }
    if($pagecheck['adminonly'] == 1) {
        $adminonly = 'checked';
    }
    if($pagecheck['editlock'] == 1) {
        $editlock = 'checked';
    }
    if(in_array($_GET['category'], ['cards','goods','chars','dice'])) {
      # Overrides for Game Items 
      /* 
      * Check if one of the game items are in the category slot
      * If yes, get the type and the item name
      */
      $type = explode('-', $_GET['page']);
      $types = [
          '3*',
          '4*',
          '5*',
          '5G',
          '5P',
          '5D',
          '6*',
          '6G',
          '6G+',
          '6P',
          '6P+',
          'black',
          'inf', 'infinity',
          'seasonal', // latest seasonal version
          'collector', 'col' // alias col
      ];
      foreach($type as $k => $v) {
          if(in_array($v, $types)) {
              $type = $v;
              $page2 = str_ireplace($v, '', $_GET['page']);
          }
      }
      $item = $cms->getItem(trim(str_ireplace('-', ' ', $page2)), $type, $_GET['category']);
      if($item) {
      $page['title'] = $item['name'];
      $page['content'] = "<blockquote>{$item['description']}</blockquote>";
      # Image Field
      $imgfield = "<div class='form-row'>
      <div class='form-group col-md-12'>
      <label for='pageTitle'>Item Image</label>
      <input type='text' class='form-control' id='pageTitle' name='title' placeholder='Page Title' value=\"{$item['image']}\">
      </div>
      </div>";
      }
    }
    $postForm = str_replace('edit', 'view', $_SERVER['REQUEST_URI']);
    print "
        <div class='modal fade' id='editModal' tabindex='-1' role='dialog' aria-labelledby='editModalLabel' aria-hidden='true'>
  <div class='modal-dialog modal-lg' role='document'>
    <div class='modal-content'>
      <div class='modal-header'>
        <h5 class='modal-title' id='editModalLabel'>Edit Page :: {$pagecheck['title']}</h5>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
      </div>
      <div class='modal-body'>
        <form action='{$_SERVER['REQUEST_URI']}' method='POST'>
        <div class='form-row'>
            <div class='form-group col-md-5'>
            <label for='pageTitle'>Title</label>
            <input type='text' class='form-control' id='pageTitle' name='title' placeholder='Page Title' value=\"{$pagecheck['title']}\">
            </div>
            <div class='form-group col-md-4'>
            <label for='pageCrumb'>Category/Crumb</label>
            <input type='text' class='form-control' id='pageCrumb' name='crumb' placeholder='category/page-name'  value=\"$crumb\">
            </div>
            <div class='form-group col-md-3'>
                <div class='form-check'>
                <input class='form-check-input' type='checkbox' name='options[editlock]' value='1' id='editLock' $editlock>
                <label class='form-check-label' for='editLock'>
                    Locked
                </label>
                </div>
                <div class='form-check'>
                <input class='form-check-input' type='checkbox' name='options[adminonly]' value='1'  id='adminOnly' $adminonly>
                <label class='form-check-label' for='adminOnly'>
                    Admin Lock
                </label>
                </div>
            </div>
        </div>
        $imgfield
        <div class='form-group'>
            <label for='pageTextArea'>Page</label>
            <textarea class='form-control' name='content' id='pageTextArea textboxes' rows='8'>{$pagecheck['content']}</textarea>
        </div>
        $tips
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
        <input type='submit' name='submit' class='btn btn-primary' value='Save changes' />
        </form>
        </div>
      </div>
  </div>
</div><script>setTimeout($('#editModal').modal(), 200);</script>
        ";
}

if($_GET['action'] == 'create') {
    if($pagecheck) {
        # Page exists, show the edit dialog instead
        header('Location: '.str_replace('create', 'edit', $_SERVER['REQUEST_URI']));
        exit();
    }
    if(!empty($_GET['category'])) {
        $crumb = str_replace(' ', '-', $_GET['category'].'/'.$_GET['page']);
    } else {
        $crumb = $_GET['page'];
        if($crumb == 'new') {
          $crumb = '';
        }
    }
    $postForm = str_replace('create', 'view', $_SERVER['REQUEST_URI']);
    print "
        <div class='modal fade' id='createModal' tabindex='-1' role='dialog' aria-labelledby='createModalLabel' aria-hidden='true'>
  <div class='modal-dialog modal-lg' role='document'>
    <div class='modal-content'>
      <div class='modal-header'>
        <h5 class='modal-title' id='createModalLabel'>Create Page</h5>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
      </div>
      <div class='modal-body'>
        <form action='{$_SERVER['REQUEST_URI']}' method='POST'>
        <div class='form-row'>
            <div class='form-group col-md-5'>
            <label for='pageTitle'>Title</label>
            <input type='text' class='form-control' id='pageTitle' name='title' placeholder='Page Title'>
            </div>
            <div class='form-group col-md-4'>
            <label for='pageCrumb'>Category/Crumb</label>
            <input type='text' class='form-control' id='pageCrumb' name='crumb' placeholder='category/page-name' value=\"$crumb\">
            </div>
            <div class='form-group col-md-3'>
                <div class='form-check'>
                <input class='form-check-input' type='checkbox' name='options[editlock]' value='1' id='editLock'>
                <label class='form-check-label' for='editLock'>
                    Locked
                </label>
                </div>
                <div class='form-check'>
                <input class='form-check-input' type='checkbox' name='options[adminonly]' value='1'  id='adminOnly'>
                <label class='form-check-label' for='adminOnly'>
                    Admin Lock
                </label>
                </div>
            </div>
        </div>
        <div class='form-group'>
            <label for='pageTextArea'>Page</label>
            <textarea class='form-control' name='content' id='pageTextArea textboxes' rows='8'></textarea>
        </div>
        $tips
      </div>
      $imgfield
      <div class='modal-footer'>
        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
        <input type='submit' name='submit' class='btn btn-primary' value='Save changes' />
        </form>
      </div>
    </div>
  </div>
</div><script>setTimeout($('#createModal').modal(), 200);</script>
        ";
    
}

if($_GET['action'] == 'lock') {
    if($pagecheck) {
        if($users->isEditor()) {
            if($pagecheck['adminonly'] && $users->getRank($_SESSION['profile']['user']) != 420) {
                # Only admins can edit these pages            
            }
        } else { # Users 
            if($pagecheck['editlock'] == 1) {
                # Users are not allowed to edit locked pages
            }
        }
    }
}

if($_GET['action'] == 'manage') {
    if($pagecheck) {
        if(!$users->isEditor()) {
            # Only editors can manage pages
        }
    }
}

/*
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button>

<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
  <div class='modal-dialog' role='document'>
    <div class='modal-content'>
      <div class='modal-header'>
        <h5 class='modal-title' id='exampleModalLabel'>Modal title</h5>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
      </div>
      <div class='modal-body'>
        ...
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
        <button type='button' class='btn btn-primary'>Save changes</button>
      </div>
    </div>
  </div>
</div>
*/
?>