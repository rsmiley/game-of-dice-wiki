<?php
# temp.php
# [Template File] | Index Page | Written by Roger Smiley
$type = ucwords($_GET['page']);
$nos = str_ireplace('s', '', $type);
?>


<div class="row">

<div class="col-lg-12">
<div class="card shadow border-info mb-4">
        <div class="card-header">
            <b>List of <i><?php echo $type; ?></i></b>
        </div>
        <div class="card-body">
            <div class="">
                <table class="table table-bordered" id="dataTable">
                    <thead>
                        <th><?php echo $nos; ?></th>
                        <th>Version</th>
                        <th>Description</th>
                        <th>Unique Page</th>
                    </thead>
                    <tbody>                
                <?php
                    $data = DB::query("SELECT * FROM items WHERE `class` = %s", $_GET['page']);
                    foreach($data as $k) {
                        $link = str_replace(' ', '-', strtolower($k['name']));
                        print "<tr>
                        <td><a href=\"/{$_GET['page']}/{$k['type']}-{$link}/\">{$k['name']}</a></td>
                        <td>{$k['type']}</td>
                        <td>{$k['description']}</td>
                        <td>No</td>
                        </tr>";
                    }
                ?>
                    </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
</div>
