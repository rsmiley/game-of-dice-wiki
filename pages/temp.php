<?php
# temp.php
# [Template File] | Index Page | Written by Roger Smiley
$page = $cms->getPage($_GET['page'], $_GET['category']);
if(!empty($_GET['category'])) { $pagecat = $_GET['category'].'/'; }
if(!$page) {
if($users->isAdmin() || $users->isEditor()) {
      print "<div class='row'><div class='col-xl-6 col-lg-12 col-md-12 mb-4'><div class='card bg-info text-white shadow'>
  <div class='card-body'><b>Admin Notice:</b> The page does not exist, want to <a href='/create/".$pagecat.$_GET['page']."/'>create it</a>?</div></div></div></div>";
  } else {
    print "<div class='row'><div class='col-xl-6 col-lg-12 col-md-12 mb-4'><div class='card bg-info text-white shadow'>
    <div class='card-body'>The page does not exist, ask an admin to create it?</div></div></div></div>";
  }
  require_once('pages/home.php');
  $skipcontent = 1;
}
if($page['html'] == 1) {
  echo $page['content'];
  $skipcontent = 1;
}
if(!$users->isLoggedIn() && $page['loggedinonly'] == 1) {
  $page['title'] = '<b>Page Restricted</b>';
  $page['content'] = "You must be <a href='/login'>logged in</a> to access this page.";
}
if($users->isLoggedIn() && $users->getRank($_SESSION['profile']['user']) != 420 && $page['adminonly'] == 1) {
  $page['title'] = '<b>Page Restricted</b>';
  $page['content'] = "You must be an administrator to access this page.";
}
if($skipcontent != 1) {
if(isset($_SESSION['echo'])) { echo $_SESSION['echo']; unset($_SESSION['echo']); }
if($users->isAdmin() || $users->isEditor()) {
  $editbtn = "<button onclick='javascript:window.location.href=\"/edit/{$pagecat}{$_GET['page']}\"' class='btn btn-primary pull-right' style='float:right;'>Edit Page</button>";
}
?>
<div class="row">

<div class="col-lg-9">
<div class="card shadow border-info mb-4">
        <div class="card-header">
            <?php echo $page['title']; echo $editbtn; ?>
        </div>
        <div class="card-body">
            <?php echo $page['content']; ?>
        </div>
    </div>
</div>
<div class="col-lg-3">
<div class="card shadow border-info mb-4">
        <div class="card-header">
            Page Info
        </div>
        <div class="card-body">
            Last Edit by <b><?php echo $page['creator']; ?></b><br />
            Last edited on <?php echo date("F j, Y", $page['lastedit']); ?><br />
            <?php
              if($page['editlock'] == 1) {
                print "<b style='color:red;'>Edit Locked</b><br />";
              }
            ?>
            <small><i>* Custom Page *</i></small>
        </div>
    </div>
</div>
</div>
<?php } ?>